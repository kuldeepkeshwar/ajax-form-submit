# ajax-form-submit

This directive can be used to submit form using ajax and show server side validation errors.

## Example
    - index.html

```html
<body ng-app='myApp' class="container">
	<div class="well" ng-controller="MyCtrl">
		<h3 class="text-center">Submit Form Using Ajax</h3>
        <form class="form-horizontal" ajax-submit="ajaxOptions" action="addPerson" method="POST" name="myform">
			<div class="form-group">
				<ajax-submit-response></ajax-submit-response>
            </div>
			<div class="form-group">
				<label for="name" class="col-sm-2 control-label">Name</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="name" name="name" ng-model="name" placeholder="Enter name">
				</div>				
			</div>
			<div class="form-group">
				<label for="age" class="col-sm-2 control-label">Age</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="age" name="age" ng-model="age" placeholder="Enter age">
				</div>				
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
				  <button type="submit" class="btn btn-success">Submit</button>
				</div>
			</div>			  
        </form>
		<p>Name can't be empty</p>
		<p>Age should be numeric</p>
    </div> 
</body>

```
    - script.js

```javascript
    angular.module('myApp', ['ajaxFormSubmitApp']);
    angular.module('myApp').controller('MyCtrl', ['$scope','$log','$q',function($scope,$log,$q) {
        $scope.name = 'Boop';
        $scope.age = 15;
        $scope.$watch('age',function(n){
            if(n==100){
              $scope.myform.age.$setValidity('UiValidationFailed', false);
            }else{
              $scope.myform.age.$setValidity('UiValidationFailed', true);
            }
        })
      	$scope.ajaxOptions={
      		pre:function(){
                var deferred = $q.defer();
                deferred.resolve();
                $log.log('pre:hook');
                return deferred.promise;
      		},
      		success:function(resp){
      			$log.log('success:hook',resp);
      		},
      		error:function(resp){
      			$log.log('error:hook',resp);
      		}
      	}
    }]);

```

    - HelloController.java

```java
    @Controller
    public class HelloWorldController { 
        @RequestMapping(value="/addPerson", method=RequestMethod.POST)
        public ResponseEntity<Object> checkPersonInfo(@Valid Person person, BindingResult bindingResult) {
        	HttpHeaders headers = new HttpHeaders();
    		headers.setContentType(MediaType.APPLICATION_JSON);
    		ValidationUtils.rejectIfEmpty(bindingResult, "name", "Name can't be empty.");
            if (bindingResult.hasErrors()) {
            	return new ResponseEntity<Object>(bindingResult.getAllErrors(),headers, HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<Object>(person, HttpStatus.OK);
        }
    }

```
    - Person.java

```java
    public class Person {
        private String name;
        private Integer age;
    
        public String getName() {
            return this.name;
        }
    
        public void setName(String name) {
            this.name = name;
        }
    
        public Integer getAge() {
            return age;
        }
    
        public void setAge(Integer age) {
            this.age = age;
        }
    
        public String toString() {
            return "Person(Name: " + this.name + ", Age: " + this.age + ")";
        }
    }

```




* **Usage**

    * *ajax-submit*           : To submit form using ajax, can be configured with pre-submit,success,error hooks
    * *ajax-submit-response*  : can be used as placeholder to show response in form
    * *ajax-submit-error*     : can be used as placeholder to show server side validation errors in form
    * *ajax-submit-success*   : can be used as placeholder to show success message in form

* **Options**

    * *pre*       :   a pre-submit hook function which returns promise, form will be submitted only after promise is resolved, if promise is rejected then stops the form submission process
    * *success*   :   handler function to process response after successful submission of form
    * *error*     :   handler function to process response(errors) 

* **Format for Server Side Error**

    - If there is any server side validation failure, 'ajax-submit' directive expect response with status-400(Bad Request) and a array of errors 
    - Error Json
        ```
        {
            field:'name',
            code:'Name can't be empty'
        }
        ```
    
    
## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.

## Demo
[Demo](http://1-dot-infinite-matter-854.appspot.com/)

