'use strict';

/**
 * @ngdoc overview
 * @name ajaxFormSubmitApp
 * @description
 * # ajaxFormSubmitApp
 *
 * Main module of the application.
 */
angular
  .module('ajaxFormSubmitApp', []);

'use strict';

/**
 * @ngdoc directive
 * @name ajaxFormSubmitApp.directive:ajaxSubmit
 * @description
 * # ajaxSubmit
 */
angular.module('ajaxFormSubmitApp')
  .directive('ajaxSubmit', ['$compile', '$http','$q',function($compile, $http,$q) {
    var _defaultPreFn=function(){
      var deferred = $q.defer();
      deferred.resolve();
      return deferred.promise;
    }
	return {
      restrict: 'A',
      require: ['form','ajaxSubmit'],
      scope: {
  		  options:'=ajaxSubmit'
  	  },
      controller:['$scope',function($scope){
        var errors=[];
        var success=false;
        this.getError=function(){
          return errors;
        }
        this.setError=function(data){
          errors=data;
        }
        this.getSuccess=function(){
          return success;
        }
        this.setSuccess=function(data){
          success=data;
        }   
      }],
      link:function(scope, element, attr, controller) {
        var _options,_processError,_processSuccess,_cleanUp,_submit,_resp,formCtrl,ajaxSubmitCtrl;
        formCtrl=controller[0];
        ajaxSubmitCtrl=controller[1];
  			var _processError=function(data){
          ajaxSubmitCtrl.setError(data);
        };
        var _processSuccess=function(data){
          ajaxSubmitCtrl.setSuccess(data);
        };
  			var _cleanUp=function(){
  				angular.forEach(formCtrl, function(e,pro) {
            if(pro.indexOf('$')==-1 && angular.isFunction(e.$setValidity))
               e.$setValidity('ServerValidationFailed', true);
          });
          ajaxSubmitCtrl.setError([]);
          ajaxSubmitCtrl.setSuccess(false);
        };
  			var _submit = function(e) {
          if (e.preventDefault)
            e.preventDefault();
          _cleanUp();
          
          if(!formCtrl.$valid) return;
          _options.pre().then(function(){
            _$submit();
          });
          return false;
        };
  			var _$submit=function(){
          var _request={
              method: attr.method,
              url: attr.action,
              data: $(element).serialize(),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          };
          
          $http(_request).success(function(data, status, headers, config) {
            _resp={
              status:status,
              data:data,
              headers:headers,
              config:config
            };
            _options.success(_resp);
            _processSuccess(true);
          }).error(function(data, status, headers, config) {
            _resp={
              status:status,
              data:data,
              headers:headers,
              config:config
            };
            _options.error(_resp);
            if(_resp.status==400)
              _processError(_resp.data);
          });
        };
  			
        var _options={
          pre:_defaultPreFn,
          success:angular.noop,
          error:angular.noop,
        };
        angular.extend(_options,scope.options);
        element.on("submit", _submit);        
    }
  };
}]);

'use strict';

/**
 * @ngdoc directive
 * @name ajaxFormSubmitApp.directive:ajaxSubmitSuccess
 * @description
 * # ajaxSubmitSuccess
 */
angular.module('ajaxFormSubmitApp')
  .directive('ajaxSubmitSuccess', function () {
    var templateFn=function(){
      return '<div ng-show="submitted" role="alert" class="alert alert-success"><button ng-click="hide()" class="close" type="button"><span aria-hidden="true">x</span><span class="sr-only">Close</span></button>'+
              '<p>{{successMsg}}</p>'+
            '</div>';
    }
    return {
      template:templateFn,
      require: ['^ajaxSubmit'],
      restrict: 'E',scope:{},
      link: function postLink(scope, element, attrs,controllers) {
          var ajaxSubmitCtrl;
          ajaxSubmitCtrl=controllers[0];
          scope.submitted=false;
          scope.successMsg = "Form submitted !!";
          scope.hideSuccess=function(){
            scope.submitted=false;
          }
          var _processSuccess=function(data){
            if(data===true){
              scope.submitted=true;
            }else{
              scope.submitted=false;
            }
          };
          scope.$watch(function(){
            return ajaxSubmitCtrl.getSuccess();
          },function(n){
              _processSuccess(n);
          });
          scope.hide=function(){
            ajaxSubmitCtrl.setSuccess(false);
          }
        }
      };
});
'use strict';

/**
 * @ngdoc directive
 * @name ajaxFormSubmitApp.directive:ajaxSubmitError
 * @description
 * # ajaxSubmitError
 */
angular.module('ajaxFormSubmitApp').directive('ajaxSubmitError',  function () {
     var templateFn=function(){
      return '<div ng-show="isError()" role="alert" class="alert alert-danger"><button ng-click="hide()" class="close" type="button"><span aria-hidden="true">x</span><span class="sr-only">Close</span></button>'+
              '<p ng-repeat="f in errors.fieldErrors"><a href="#{{f.field}}">{{f.code}}</a></p>'+
              '<p ng-repeat="g in errors.globalErrors">{{g.code}}</p>'+
            '</div>';
    }
    return {
      template:templateFn,
      require: ['^ajaxSubmit','^form'],
      restrict: 'E',scope:{},
      link: function postLink(scope, element, attrs,controllers) {
          var ajaxSubmitCtrl,formCtrl;
          ajaxSubmitCtrl=controllers[0];
          formCtrl=controllers[1];
          scope.errors ={};
          scope.errors.fieldErrors =[];
          scope.errors.globalErrors =[];
          var _removeError=function(){
              angular.forEach(scope.errors.fieldErrors, function(e, index) {
                if(formCtrl[e.field])
                   formCtrl[e.field].$setValidity('ServerValidationFailed', true);
              });
              scope.errors.fieldErrors =[];
              scope.errors.globalErrors =[];
          }
          var _processError=function(data){
            //temp hack as error format is not standardized
            if(!_$isProcessed(data)){
              _removeError();
            }            
          };
          var _$isProcessed=function(data){
            if(!data){
              return false;
            }
            //check if response has fieldError/globalErrors property
            if(angular.isObject(data['fieldErrors']) || angular.isObject(data['globalErrors'])){
              angular.forEach(data['fieldErrors'], function(value, key) {
                scope.errors.fieldErrors.push({
                  field:key,
                  code:value
                });
                if(formCtrl[key])
                   formCtrl[key].$setValidity('ServerValidationFailed', false);
              });
              angular.forEach(data['globalErrors'], function(value, key) {
                scope.errors.globalErrors.push({
                  field:key,
                  code:value||key
                });
              });
              return true;              
            }else if(angular.isArray(data)){ //check if response is a list of errors
              angular.forEach(data, function(e) {
                if(formCtrl[e.field]){
                  formCtrl[e.field].$setValidity('ServerValidationFailed', false); 
                  scope.errors.fieldErrors.push({
                    field:e.field,
                    code:e.defaultMessage||e.code
                  });
                }else{
                  scope.errors.globalErrors.push({
                    field:e.field,
                    code:e.defaultMessage||e.code
                  });
                }                
              });
              return true;
            }else if(angular.isString(data)){//check if response is a error message
              scope.errors.globalErrors.push({
                field:"",
                code:data
              });
              return true;
            }else if(angular.isObject(data)){//check if response is a object with key as error code and value as error message
              angular.forEach(data, function(value, key) {
                if(formCtrl[key]){
                  formCtrl[key].$setValidity('ServerValidationFailed', false);
                  scope.errors.fieldErrors.push({
                    field:key,
                    code:value
                  });
                }else{
                  scope.errors.globalErrors.push({
                    field:key,
                    code:value
                  });
                }
              });
              return true;
            }
            return false;
          }
          scope.$watch(function(){
            return ajaxSubmitCtrl.getError();
          },function(n){
              _processError(n);
          });
          scope.hide=function(){
            ajaxSubmitCtrl.setError();
          }
          scope.isError=function(){
            return (scope.errors.fieldErrors.length>0||scope.errors.globalErrors.length>0)
          }
        }
      };
});

'use strict';

/**
 * @ngdoc directive
 * @name ajaxFormSubmitApp.directive:ajaxSubmitResponse
 * @description
 * # ajaxSubmitResponse
 */
angular.module('ajaxFormSubmitApp')
  .directive('ajaxSubmitResponse', function () {
    return {
      template:"<ajax-submit-error></ajax-submit-error><ajax-submit-success></ajax-submit-success>",
      require: ['^ajaxSubmit']
    };
  });
