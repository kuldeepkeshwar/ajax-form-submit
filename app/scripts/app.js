'use strict';

/**
 * @ngdoc overview
 * @name ajaxFormSubmitApp
 * @description
 * # ajaxFormSubmitApp
 *
 * Main module of the application.
 */
angular
  .module('ajaxFormSubmitApp', []);
