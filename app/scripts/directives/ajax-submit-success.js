'use strict';

/**
 * @ngdoc directive
 * @name ajaxFormSubmitApp.directive:ajaxSubmitSuccess
 * @description
 * # ajaxSubmitSuccess
 */
angular.module('ajaxFormSubmitApp')
  .directive('ajaxSubmitSuccess', function () {
    var templateFn=function(){
      return '<div ng-show="submitted" role="alert" class="alert alert-success"><button ng-click="hide()" class="close" type="button"><span aria-hidden="true">x</span><span class="sr-only">Close</span></button>'+
              '<p>{{successMsg}}</p>'+
            '</div>';
    }
    return {
      template:templateFn,
      require: ['^ajaxSubmit'],
      restrict: 'E',scope:{},
      link: function postLink(scope, element, attrs,controllers) {
          var ajaxSubmitCtrl;
          ajaxSubmitCtrl=controllers[0];
          scope.submitted=false;
          scope.successMsg = "Form submitted !!";
          scope.hideSuccess=function(){
            scope.submitted=false;
          }
          var _processSuccess=function(data){
            if(data===true){
              scope.submitted=true;
            }else{
              scope.submitted=false;
            }
          };
          scope.$watch(function(){
            return ajaxSubmitCtrl.getSuccess();
          },function(n){
              _processSuccess(n);
          });
          scope.hide=function(){
            ajaxSubmitCtrl.setSuccess(false);
          }
        }
      };
});