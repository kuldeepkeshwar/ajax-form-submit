'use strict';

/**
 * @ngdoc directive
 * @name ajaxFormSubmitApp.directive:ajaxSubmitResponse
 * @description
 * # ajaxSubmitResponse
 */
angular.module('ajaxFormSubmitApp')
  .directive('ajaxSubmitResponse', function () {
    return {
      template:"<ajax-submit-error></ajax-submit-error><ajax-submit-success></ajax-submit-success>",
      require: ['^ajaxSubmit']
    };
  });
