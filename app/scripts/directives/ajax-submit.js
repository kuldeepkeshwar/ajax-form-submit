'use strict';

/**
 * @ngdoc directive
 * @name ajaxFormSubmitApp.directive:ajaxSubmit
 * @description
 * # ajaxSubmit
 */
angular.module('ajaxFormSubmitApp')
  .directive('ajaxSubmit', ['$compile', '$http','$q',function($compile, $http,$q) {
    var _defaultPreFn=function(){
      var deferred = $q.defer();
      deferred.resolve();
      return deferred.promise;
    }
	return {
      restrict: 'A',
      require: ['form','ajaxSubmit'],
      scope: {
  		  options:'=ajaxSubmit'
  	  },
      controller:['$scope',function($scope){
        var errors=[];
        var success=false;
        this.getError=function(){
          return errors;
        }
        this.setError=function(data){
          errors=data;
        }
        this.getSuccess=function(){
          return success;
        }
        this.setSuccess=function(data){
          success=data;
        }   
      }],
      link:function(scope, element, attr, controller) {
        var _options,_processError,_processSuccess,_cleanUp,_submit,_resp,formCtrl,ajaxSubmitCtrl;
        formCtrl=controller[0];
        ajaxSubmitCtrl=controller[1];
  			var _processError=function(data){
          ajaxSubmitCtrl.setError(data);
        };
        var _processSuccess=function(data){
          ajaxSubmitCtrl.setSuccess(data);
        };
  			var _cleanUp=function(){
  				angular.forEach(formCtrl, function(e,pro) {
            if(pro.indexOf('$')==-1 && angular.isFunction(e.$setValidity))
               e.$setValidity('ServerValidationFailed', true);
          });
          ajaxSubmitCtrl.setError([]);
          ajaxSubmitCtrl.setSuccess(false);
        };
  			var _submit = function(e) {
          if (e.preventDefault)
            e.preventDefault();
          _cleanUp();
          
          if(!formCtrl.$valid) return;
          _options.pre().then(function(){
            _$submit();
          });
          return false;
        };
  			var _$submit=function(){
          var _request={
              method: attr.method,
              url: attr.action,
              data: $(element).serialize(),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          };
          
          $http(_request).success(function(data, status, headers, config) {
            _resp={
              status:status,
              data:data,
              headers:headers,
              config:config
            };
            _options.success(_resp);
            _processSuccess(true);
          }).error(function(data, status, headers, config) {
            _resp={
              status:status,
              data:data,
              headers:headers,
              config:config
            };
            _options.error(_resp);
            if(_resp.status==400)
              _processError(_resp.data);
          });
        };
  			
        var _options={
          pre:_defaultPreFn,
          success:angular.noop,
          error:angular.noop,
        };
        angular.extend(_options,scope.options);
        element.on("submit", _submit);        
    }
  };
}]);
