'use strict';

/**
 * @ngdoc directive
 * @name ajaxFormSubmitApp.directive:ajaxSubmitError
 * @description
 * # ajaxSubmitError
 */
angular.module('ajaxFormSubmitApp').directive('ajaxSubmitError',  function () {
     var templateFn=function(){
      return '<div ng-show="isError()" role="alert" class="alert alert-danger"><button ng-click="hide()" class="close" type="button"><span aria-hidden="true">x</span><span class="sr-only">Close</span></button>'+
              '<p ng-repeat="f in errors.fieldErrors"><a href="#{{f.field}}">{{f.code}}</a></p>'+
              '<p ng-repeat="g in errors.globalErrors">{{g.code}}</p>'+
            '</div>';
    }
    return {
      template:templateFn,
      require: ['^ajaxSubmit','^form'],
      restrict: 'E',scope:{},
      link: function postLink(scope, element, attrs,controllers) {
          var ajaxSubmitCtrl,formCtrl;
          ajaxSubmitCtrl=controllers[0];
          formCtrl=controllers[1];
          scope.errors ={};
          scope.errors.fieldErrors =[];
          scope.errors.globalErrors =[];
          var _removeError=function(){
              angular.forEach(scope.errors.fieldErrors, function(e, index) {
                if(formCtrl[e.field])
                   formCtrl[e.field].$setValidity('ServerValidationFailed', true);
              });
              scope.errors.fieldErrors =[];
              scope.errors.globalErrors =[];
          }
          var _processError=function(data){
            //temp hack as error format is not standardized
            if(!_$isProcessed(data)){
              _removeError();
            }            
          };
          var _$isProcessed=function(data){
            if(!data){
              return false;
            }
            //check if response has fieldError/globalErrors property
            if(angular.isObject(data['fieldErrors']) || angular.isObject(data['globalErrors'])){
              angular.forEach(data['fieldErrors'], function(value, key) {
                scope.errors.fieldErrors.push({
                  field:key,
                  code:value
                });
                if(formCtrl[key])
                   formCtrl[key].$setValidity('ServerValidationFailed', false);
              });
              angular.forEach(data['globalErrors'], function(value, key) {
                scope.errors.globalErrors.push({
                  field:key,
                  code:value||key
                });
              });
              return true;              
            }else if(angular.isArray(data)){ //check if response is a list of errors
              angular.forEach(data, function(e) {
                if(formCtrl[e.field]){
                  formCtrl[e.field].$setValidity('ServerValidationFailed', false); 
                  scope.errors.fieldErrors.push({
                    field:e.field,
                    code:e.defaultMessage||e.code
                  });
                }else{
                  scope.errors.globalErrors.push({
                    field:e.field,
                    code:e.defaultMessage||e.code
                  });
                }                
              });
              return true;
            }else if(angular.isString(data)){//check if response is a error message
              scope.errors.globalErrors.push({
                field:"",
                code:data
              });
              return true;
            }else if(angular.isObject(data)){//check if response is a object with key as error code and value as error message
              angular.forEach(data, function(value, key) {
                if(formCtrl[key]){
                  formCtrl[key].$setValidity('ServerValidationFailed', false);
                  scope.errors.fieldErrors.push({
                    field:key,
                    code:value
                  });
                }else{
                  scope.errors.globalErrors.push({
                    field:key,
                    code:value
                  });
                }
              });
              return true;
            }
            return false;
          }
          scope.$watch(function(){
            return ajaxSubmitCtrl.getError();
          },function(n){
              _processError(n);
          });
          scope.hide=function(){
            ajaxSubmitCtrl.setError();
          }
          scope.isError=function(){
            return (scope.errors.fieldErrors.length>0||scope.errors.globalErrors.length>0)
          }
        }
      };
});
