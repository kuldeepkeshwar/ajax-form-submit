'use strict';

describe('Directive: ajaxSubmitError', function () {

  // load the directive's module
  beforeEach(module('ajaxFormSubmitApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<ajax-submit-error></ajax-submit-error>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the ajaxSubmitError directive');
  }));
});
