'use strict';

describe('Directive: ajaxSubmit', function () {

  // load the directive's module
  beforeEach(module('ajaxFormSubmitApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<ajax-submit></ajax-submit>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the ajaxSubmit directive');
  }));
});
